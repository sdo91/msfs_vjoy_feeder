﻿/////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This project demonstrates how to write a simple vJoy feeder in C#
//
// The ROBUST section demonstrate the usage of functions that are easy and safe to use but are less efficient
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace FeederDemoCS
{
    class RobustDemo : BaseFeeder
    {
        public RobustDemo(string[] args) : base(args)
        {
            //
        }

        public override void Run()
        {
            bool res;
            // Reset this device to default values
            joystick.ResetVJD(id);

            // Feed the device in endless loop
            while (true)
            {
                // Set position of 4 axes
                res = joystick.SetAxis(X, id, HID_USAGES.HID_USAGE_X);
                res = joystick.SetAxis(Y, id, HID_USAGES.HID_USAGE_Y);
                res = joystick.SetAxis(Z, id, HID_USAGES.HID_USAGE_Z);
                res = joystick.SetAxis(XR, id, HID_USAGES.HID_USAGE_RX);
                res = joystick.SetAxis(ZR, id, HID_USAGES.HID_USAGE_RZ);

                // Press/Release Buttons
                res = joystick.SetBtn(true, id, count / 50);
                res = joystick.SetBtn(false, id, 1 + count / 50);

                // If Continuous POV hat switches installed - make them go round
                // For high values - put the switches in neutral state
                if (ContPovNumber > 0)
                {
                    if ((count * 70) < 30000)
                    {
                        res = joystick.SetContPov(((int)count * 70), id, 1);
                        res = joystick.SetContPov(((int)count * 70) + 2000, id, 2);
                        res = joystick.SetContPov(((int)count * 70) + 4000, id, 3);
                        res = joystick.SetContPov(((int)count * 70) + 6000, id, 4);
                    }
                    else
                    {
                        res = joystick.SetContPov(-1, id, 1);
                        res = joystick.SetContPov(-1, id, 2);
                        res = joystick.SetContPov(-1, id, 3);
                        res = joystick.SetContPov(-1, id, 4);
                    };
                };

                // If Discrete POV hat switches installed - make them go round
                // From time to time - put the switches in neutral state
                if (DiscPovNumber > 0)
                {
                    if (count < 550)
                    {
                        joystick.SetDiscPov((((int)count / 20) + 0) % 4, id, 1);
                        joystick.SetDiscPov((((int)count / 20) + 1) % 4, id, 2);
                        joystick.SetDiscPov((((int)count / 20) + 2) % 4, id, 3);
                        joystick.SetDiscPov((((int)count / 20) + 3) % 4, id, 4);
                    }
                    else
                    {
                        joystick.SetDiscPov(-1, id, 1);
                        joystick.SetDiscPov(-1, id, 2);
                        joystick.SetDiscPov(-1, id, 3);
                        joystick.SetDiscPov(-1, id, 4);
                    };
                };

                System.Threading.Thread.Sleep(20);
                X += 150; if (X > maxval) X = 0;
                Y += 250; if (Y > maxval) Y = 0;
                Z += 350; if (Z > maxval) Z = 0;
                XR += 220; if (XR > maxval) XR = 0;
                ZR += 200; if (ZR > maxval) ZR = 0;
                count++;

                if (count > 640)
                    count = 0;

            } // While (Robust)
        }
    }
}
