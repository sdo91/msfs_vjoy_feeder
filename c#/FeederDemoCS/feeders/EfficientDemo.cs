﻿/////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This project demonstrates how to write a simple vJoy feeder in C#
//
// The EFFICIENT section demonstrate the usage of functions that are more efficient
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;

namespace FeederDemoCS
{
    class EfficientDemo : BaseFeeder
    {
        public EfficientDemo(string[] args) : base(args)
        {
            //
        }

        public override void Run()
        {
            byte[] pov = new byte[4];

            while (true)
            {
                iReport.bDevice = (byte)id;
                iReport.AxisX = X;
                iReport.AxisY = Y;
                iReport.AxisZ = Z;
                iReport.AxisZRot = ZR;
                iReport.AxisXRot = XR;

                // Set buttons one by one
                iReport.Buttons = (uint)(0x1 << (int)(count / 20));

                if (ContPovNumber > 0)
                {
                    // Make Continuous POV Hat spin
                    iReport.bHats = (count * 70);
                    iReport.bHatsEx1 = (count * 70) + 3000;
                    iReport.bHatsEx2 = (count * 70) + 5000;
                    iReport.bHatsEx3 = 15000 - (count * 70);
                    if ((count * 70) > 36000)
                    {
                        iReport.bHats = 0xFFFFFFFF; // Neutral state
                        iReport.bHatsEx1 = 0xFFFFFFFF; // Neutral state
                        iReport.bHatsEx2 = 0xFFFFFFFF; // Neutral state
                        iReport.bHatsEx3 = 0xFFFFFFFF; // Neutral state
                    };
                }
                else
                {
                    // Make 5-position POV Hat spin

                    pov[0] = (byte)(((count / 20) + 0) % 4);
                    pov[1] = (byte)(((count / 20) + 1) % 4);
                    pov[2] = (byte)(((count / 20) + 2) % 4);
                    pov[3] = (byte)(((count / 20) + 3) % 4);

                    iReport.bHats = (uint)(pov[3] << 12) | (uint)(pov[2] << 8) | (uint)(pov[1] << 4) | (uint)pov[0];
                    if ((count) > 550)
                        iReport.bHats = 0xFFFFFFFF; // Neutral state
                };

                /*** Feed the driver with the position packet - is fails then wait for input then try to re-acquire device ***/
                if (!joystick.UpdateVJD(id, ref iReport))
                {
                    Console.WriteLine("Feeding vJoy device number {0} failed - try to enable device then press enter\n", id);
                    Console.ReadKey(true);
                    joystick.AcquireVJD(id);
                }

                System.Threading.Thread.Sleep(20);
                count++;
                if (count > 640) count = 0;

                X += 150; if (X > maxval) X = 0;
                Y += 250; if (Y > maxval) Y = 0;
                Z += 350; if (Z > maxval) Z = 0;
                XR += 220; if (XR > maxval) XR = 0;
                ZR += 200; if (ZR > maxval) ZR = 0;

            }; // While
        }
    }
}
